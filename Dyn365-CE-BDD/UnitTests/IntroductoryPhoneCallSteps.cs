﻿using System;
using Microsoft.Dynamics365.UIAutomation.Browser;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Dynamics365.UIAutomation.Api;
using Microsoft.Dynamics365.UIAutomation.Sample;
using Microsoft.Dynamics365.UIAutomation.Sample.UCI;
using Microsoft.Dynamics365.UIAutomation.Sample.Web;
using System.Security;
using TechTalk.SpecFlow;


namespace UnitTests
{
    [Binding]
    public class IntroductoryPhoneCallSteps
    {
        private readonly SecureString _username = System.Configuration.ConfigurationManager.AppSettings["OnlineUsername"].ToSecureString();
        private readonly SecureString _password = System.Configuration.ConfigurationManager.AppSettings["OnlinePassword"].ToSecureString();
        private readonly Uri _xrmUri = new Uri(System.Configuration.ConfigurationManager.AppSettings["OnlineCrmUrl"].ToString());

        //public object TestSettings { get; private set; }

        [Given(@"I login to CRM")]
        public void GivenILoginToCRM()
        {
            //using (var xrmBrowser = new XrmBrowser(_options))
            using (var xrmBrowser = new Browser(TestSettings.Options))
            {
                xrmBrowser.LoginPage.Login(_xrmUri, _username, _password);
                xrmBrowser.GuidedHelp.CloseGuidedHelp();

                xrmBrowser.ThinkTime(500);
                xrmBrowser.Navigation.OpenSubArea("Sales", "Accounts");

                xrmBrowser.ThinkTime(2000);
                xrmBrowser.Grid.SwitchView("Active Accounts");

                xrmBrowser.ThinkTime(1000);
                xrmBrowser.CommandBar.ClickCommand("New");

                xrmBrowser.ThinkTime(5000);
                xrmBrowser.Entity.SetValue("name", "Test API Account");
                xrmBrowser.Entity.SetValue("telephone1", "555-555-5555");
                xrmBrowser.Entity.SetValue("websiteurl", "https://easyrepro.crm.dynamics.com");

                xrmBrowser.CommandBar.ClickCommand("Save & Close");
                xrmBrowser.ThinkTime(2000);

            }
        }


        [When(@"I create a new account")]
        public void WhenICreateANewAccount()
        {
           //// Entity account = new Entity("account");
           // account.Attributes["name"] = "Wael";
           // account["telephone1"] = "123456789";

           // ctx.ExecutePluginWithTarget<AccountPostCreate>(account);
        }
        
        [Then(@"a phone call record should be created")]
        public void ThenAPhoneCallRecordShouldBeCreated()
        {
            //var phonecalls = ctx.CreateQuery("phonecall").ToList<Entity>();
            //Assert.AreEqual(1, phonecalls.Count);
            //phonecall = phonecalls[0];
        }
        
        [Then(@"due date should be in (.*) days")]
        public void ThenDueDateShouldBeInDays(int days)
        {
            //DateTime dueDate = (DateTime)phonecall["scheduledend"];
            //DateTime createdOn = (DateTime)phonecall["createdon"];
            //Assert.AreEqual(dueDate.Date, createdOn.Date.AddDays(days));
        }
    }
}
